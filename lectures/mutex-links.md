Планировщики / follow-up

- https://github.com/mit-pdos/xv6-public/blob/master/trap.c#L37
- https://github.com/mit-pdos/xv6-public/blob/master/swtch.S
- https://github.com/mit-pdos/xv6-public/blob/master/proc.c#L323

https://github.com/torvalds/linux/blob/b719ae070ee2371c37d846616ef7453ec6811990/kernel/sched/core.c#L4000

----

GIL in Python

https://github.com/python/cpython/blob/1c7b14197b10924e2efc1e6c99c720958be1f681/Python/ceval.c#L1098

----

Простейший Test-and-Set спинлок: 
https://github.com/mit-pdos/xv6-public/blob/master/spinlock.c#L25

----

Кэши

https://gist.github.com/jboner/2841832
https://colin-scott.github.io/personal_website/research/interactive_latency.html

https://twitter.com/holly_cummins/status/530372145025908737?lang=ru
https://netopyr.com/2014/11/28/reactions-to-the-beer-cache-hierarchy/

----

Test-and-TAS

https://github.com/catboost/catboost/blob/master/util/system/spinlock.h

https://probablydance.com/2019/12/30/measuring-mutexes-spinlocks-and-how-bad-the-linux-scheduler-really-is/

----

pause instruction
https://c9x.me/x86/html/file_module_x86_id_232.html

https://www.intel.com/content/dam/www/public/us/en/documents/manuals/64-ia-32-architectures-optimization-manual.pdf

-----

SpinWait

https://github.com/dotnet/runtime/blob/4f9ae42d861fcb4be2fcd5d3d55d5f227d30e723/src/libraries/System.Private.CoreLib/src/System/Threading/SpinWait.cs

https://github.com/catboost/catboost/blob/master/util/system/spin_wait.cpp

-----

https://aloiskraus.wordpress.com/2018/06/16/why-skylakex-cpus-are-sometimes-50-slower-how-intel-has-broken-existing-code/

https://www.tabsoverspaces.com/233735-how-is-thread-spinwait-actually-implemented

-----

Futex

https://github.com/torvalds/linux/blob/master/kernel/futex.c
https://eli.thegreenplace.net/2018/basics-of-futexes/

C++20 atomic::wait
https://en.cppreference.com/w/cpp/atomic/atomic/wait

LockSupport в Java
https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/LockSupport.html

----

https://github.com/torvalds/linux/blob/master/kernel/locking/mutex.c#L638

----

https://github.com/facebook/folly/blob/master/folly/synchronization/DistributedMutex.h

----

https://static.googleusercontent.com/media/research.google.com/en//archive/chubby-osdi06.pdf