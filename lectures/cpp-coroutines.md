# C++ Coroutines Design

- https://github.com/GorNishanov/await
- [Asymmetric Transfer](https://lewissbaker.github.io/)
- https://github.com/lewissbaker/cppcoro
- [Customization Points](https://hackmd.io/@redbeard0531/S1H_loeA7?type=view)
- [Add coroutine task type](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p1056r0.html)

## C++ IS

- https://eel.is/c++draft/dcl.fct.def.coroutine
- https://eel.is/c++draft/expr.await

## Core Coroutines

- [Coroutines TS Use Cases and Design Issues](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p0973r0.pdf)
- [Core Coroutines Proposal](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p1063r2.pdf)
- [Abseil Blog / Coroutine Types](https://abseil.io/blog/20180713-coroutine-types)

----

- [Incremental Approach: Coroutine TS + Core Coroutines](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p1362r0.pdf)
- [Better keywords for the Coroutines](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p1485r1.html)

## Examples 

- https://github.com/facebook/folly/blob/master/folly/futures/Future.h#L2652
- [Naked coroutines live](https://github.com/GorNishanov/await/tree/master/2017_CppCon/live)
- https://github.com/toby-allsopp/coroutine_monad/blob/master/maybe.h